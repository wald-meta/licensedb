# Making Changes

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "edit-"
}

```

- [ ] POST /edits                  201 Created                 This endpoint makes changes to the database, the response should have a Location header to the recorded edit
- [ ] POST /user/<id>              303 See Other               Change user settings
- [ ] POST /login                  [text/html]                 Login confirmation, confirms where the login link was sent to
