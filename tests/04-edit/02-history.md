# Edit History

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "edit-"
}

```

- [ ] GET /edit/<id>               303 See Other               <id> is an edit identifier in zbase32.  Content negotiate, redirect to one of the following:
- [ ] GET /edit/<id>.html          [text/html]                 Human readable page about this edit
- [ ] GET /edit/<id>.ttl           [text/turtle]               Turtle version of the edit
- [ ] GET /edit/<id>.jsonld        [application/ld+json]       JSON-LD version of the edit

