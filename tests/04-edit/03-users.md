### User information

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "user-"
}
```

- [ ] GET /me                      303 See Other               Use this to retrieve information about the current user, will redirect to /user/<id>
- [ ] GET /user/<id>               303 See Other               <id> is a user identifier in zbase32.  Content negotiate, redirect to one of the following:
- [ ] GET /user/<id>.html          [text/html]                 Human readable page about this user
- [ ] GET /user/<id>.ttl           [text/turtle]               Turtle version of the user
- [ ] GET /user/<id>.jsonld        [application/ld+json]       JSON-LD version of the user
- [ ] GET /login                   [text/html]                 Login screen (use passwordless), enter an email address or phone number to receive a link to login
