### Data Dumps

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "data-"
}
```

Database dumps should be available in hdt, nt and ttl formats.


```yaml
GET: /dl/
```

```yaml
status: 200
headers:
  Content-Type: text/html; charset=utf-8
body:
  - regex: licensedb.hdt
  - regex: licensedb.nt
  - regex: licensedb.ttl
```

Let's check that the web service is well behaved and can resume large downloads.  Each dump should
be well over 1 MB in size, so let's request 100 bytes from the first megabyte.

FIXME: currently disabled, because either:
- the range request isn't specified correctly, or
- wald:test (i.e. python-requests) doesn't support ranges, or
- Caddy doesn't support ranges

```yaml
skip: true
GET: /dl/licensedb.nt
Range: bytes=1048476-1048576
```

```yaml
status: 206
headers:
  Content-Type: application/n-triples
```

Instead of the range check, let's do a few simple HEAD checks:

```yaml
HEAD: /dl/licensedb.nt
```

```yaml
status: 200
headers:
  Content-Type: application/n-triples
```



```yaml
HEAD: /dl/licensedb.ttl
```

```yaml
status: 200
headers:
  Content-Type: text/turtle
```



```yaml
HEAD: /dl/licensedb.hdt
```

```yaml
status: 200
headers:
  Content-Type: application/vnd.hdt
```


