
# Static pages: Vocabulary

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "vocab-"
}

```


## Vocabulary

License records in the License database use existing terms from e.g.
[dublin core](http://dublincore.org/) and [schema.org](http://schema.org/) as much as
possible, but still need a few extra terms which are specified in the LicenseDB
vocabulary.

The LicenseDB vocabulary has HTML, Turtle and JSON-LD versions, which each can be
requested by retrieving `/ns` and setting the appropriate `Accept` header.

### text/turtle redirect

For example, if the client prefers text/turtle:

```yaml
GET: /ns
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
```

The client should be redirected:

```yaml
status: [301, 302, 303]
headers:
  Location: /ns.ttl
```

### Vocabulary as text/turtle

Following the redirect:

```yaml
GET: /ns.ttl
```

Should result in the `text/turtle` version of the vocabulary document:

```yaml
status: 200
headers:
  Content-Type: text/turtle
body:
  - regex: ^@prefix
```

### text/html redirect

Or if the client prefers text/html:

```yaml
GET: /ns
headers:
  Accept: text/turtle;q=0.3,text/html;q=0.7
```

The client should be redirected:

```yaml
status: [301, 302, 303]
headers:
  Location: /ns.html
```

### Vocabulary as html

Following the redirect:

```yaml
GET: /ns.html
```

Should result in the `text/html` version of the vocabulary document:

```yaml
status: 200
headers:
  Content-Type: text/html
body:
  - regex: ^<!DOCTYPE html>
```

### JSON-LD redirect

Or perhaps the client prefers JSON-LD:

```yaml
GET: /ns
headers:
  Accept: text/turtle;q=0.3,text/html;q=0.7,application/ld+json;q=0.9
```

The client should be redirected:

```yaml
status: [301, 302, 303]
headers:
  Location: /ns.jsonld
```

### Vocabulary as JSON-LD

Following the redirect:

```yaml
GET: /ns.jsonld
```

Should result in the JSON-LD version of the vocabulary document:

```yaml
status: 200
headers:
  Content-Type: application/ld+json
body:
  - regex: ^{
  - regex: "@context"
```

