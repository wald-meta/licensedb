
# Static pages: Miscellaneous

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "static-"
}

```


### Database

Human readable license pages are at URLs like
[/id/GPL-3.html](https://licensedb.org/id/GPL-3.html).  The database as a whole is
described at [/id](https://licensedb.org/id/). It mentions that you can query the
database using Linked Data Fragments or download a [full data dump](../05-dump.html).


```yaml
GET: /id/
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
```

The id page only has a text/html version, so that should be returned:

```yaml
status: 200
headers:
  Content-Type: text/html
body:
  - regex: Linked\s+Data\s+Fragments
  - regex: database\s+dumps
```

### License

The [/license.html](https://licensedb.org/license) page should describe that the database itself is available under CC0 and the software under Apachev2.


```yaml
GET: /license.html
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
```

The id page only has a text/html version, so that should be returned:

```yaml
status: 200
headers:
  Content-Type: text/html
body:
  - regex: CC0\s+1.0\s+Universal\s+Public\s+Domain\s+Dedication
  - regex: Apache\s+License,\s+Version\s+2.0
```

