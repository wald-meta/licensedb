
# Static pages: Welcome

LicenseDB is a project which aims to provide information about commonly used free and
open source software copyright licenses as linked data.  Which means it not only
should provide this data in commonly used RDF formats like turtle and JSON-LD, but the
website itself should also properly respond to content-negotation so that clients can
request the information from LicenseDB in the format they prefer.

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "welcome-"
}

```


### Welcome

The landing page for the License Database project for now is a simple page describing the project,
which will be moved to an /about page if we ever add a more interesting or dynamic home page.

```yaml
GET: /
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
```

The about page only has a text/html, so that should be returned:

```yaml
status: 200
headers:
  Content-Type: text/html
body:
  - regex: Welcome to the License Database project
```

