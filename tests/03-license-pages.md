
# License Pages

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "id-"
}

```


## httpRange-14

To distinguish the identifier of a particular license from the identifier of an information
resource about that license, the License Database project uses the 303 See Other
HTTP Status Code.  See also [httpRange-14](https://en.wikipedia.org/wiki/HTTPRange-14).

A license in the License Database is identified by a URL which looks like
https://licensedb.org/id/:id (where :id is an identifier such as "MIT").  This
resource will 303 redirect to an information resource about the requested license.

For example:

- `https://licensedb.org/id/MIT` identifies the MIT license
- `https://licensedb.org/id/MIT.html` is a web page about the MIT license
- `https://licensedb.org/id/MIT.json` is a JSON-LD document about the MIT license
- `https://licensedb.org/id/MIT.ttl` is a text/turtle document about the MIT license


### MIT license identifier

Resolving the fully qualified identifier of a license should result in a 303 See Other status.

```yaml
GET: /id/MIT
```

The client should be redirected:

```yaml
status: [303]
headers:
  Location: /id/MIT.html
```

### content-negotiate

While .html is the default redirect when resolving a license identifier, the client can
content-negotiate a different format, such as JSON-LD:

```yaml
GET: /id/MIT
headers:
  Accept: text/turtle;q=0.3,text/html;q=0.7,application/ld+json;q=0.9
```

```yaml
status: [303]
headers:
  Location: /id/MIT.jsonld
```

And retrieving that page should result in a JSON-LD document:

```yaml
GET: /id/MIT.jsonld
```

```yaml
status: 200
headers:
  Content-Type: application/ld+json
body:
  - regex: ^{
  - regex: "@context"
```

Or text/turtle:

```yaml
GET: /id/MIT
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
```

```yaml
status: [303]
headers:
  Location: /id/MIT.ttl
```

And ofcourse retrieving that page should result in a text/turtle document:

```yaml
GET: /id/MIT.ttl
```

```yaml
status: 200
headers:
  Content-Type: text/turtle
body:
  - regex: ^@prefix
```

### Trailing backslash

A trailing backslash added by accident should redirect to the canonical identifier with
a 301 Moved Permanently redirect.

```yaml
GET: /id/MIT/
```

The client should be redirected:

```yaml
status: [301]
headers:
  Location: https://licensedb.org/id/MIT
```

