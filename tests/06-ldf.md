# Linked Data Fragments

```json
{
    "baseUrl": "https://licensedb.org",
    "idPrefix": "ldf-"
}

```

## Dataset

Requesting /data should list the available datasets on the LDF server, which should be just
the current LicenseDB dataset.

```yaml
GET: /data
```

```yaml
status: 200
body:
  - regex: Available datasets
```

We can query the Linked Data Fragments server for the available datasets, which should
include the dataset index and the LicenseDB dataset.

```yaml
GET: /data?predicate=http%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23label
headers:
  Accept: application/n-triples
```

```json
{
    "status": 200,
    "body": [
       { "regex": "\"dataset index\"" },
       { "regex": "\"LicenseDB" }
   ]
}
```

We can query the LicenseDB dataset at https://licensedb.org/data/licensedb, for example to get
the title of the MIT license:

```yaml
GET: /data/licensedb?subject=https%3A%2F%2Flicensedb.org%2Fid%2FMIT&predicate=http%3A%2F%2Fpurl.org%2Fdc%2Fterms%2Ftitle
headers:
  Accept: application/n-triples
```

```json
{
    "status": 200,
    "body": [
       { "regex": "\\<https\\://licensedb\\.org/id/MIT\\> \\<http\\://purl\\.org/dc/terms/title\\> \"The MIT License\"\\." }
   ]
}
```
